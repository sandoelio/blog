
<?php 
    $arquivosPostagens = 'postagens.json'; 

    $postagens = [];
    if (file_exists($arquivosPostagens)) {
        $postagens = json_decode(file_get_contents($arquivosPostagens), true);
    }
  ?>
<!DOCTYPE html>
  <html>
    <head>
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link type="text/css" rel="stylesheet" href="./node_modules/materialize-css/dist/css/materialize.min.css" 
      media="screen,projection"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <style>
        body:{
          display: flex;
          min-height: 100vh;
          flex-direction: column;
        }
        main:{
          flex: 1 0 auto;
        }
      </style>
    </head>

    <body>
      <nav class="cyan">
        <div class="nav-wrapper">
          <a href="#" class="brand-logo center">FotoLog</a>
          <ul id="nav-mobile" class="left hide-on-med-and-down">
            <li><a href="postagem.php">Nova postagem</a></li>
            <li><a href="usuarios.php">Usuario</a></li>
            <li class="active"><a href="fotolog.php">Fotos</a></li>
          </ul>
        </div>
      </nav>
      <main>
        <div class="container " style= "margin-top: 50px">
          <?php
            if (count($postagens)) {
              foreach ($postagens as $i => $p) {
              if ($i % 2 == 0) 
                echo '<div class="row">';
              echo '<div class="col s4 offset-s1">';
              echo '    <div class="card grey lighten-5 z-depth-3">';
              echo '        <div class="card-image">';
              echo '            <img src="'. $p['foto'] .'">';
              echo '            <span class="card-title">'. $p['titulo'] .'</span>';
              echo '        </div>';
              echo '        <div class="card-content">';
              echo '            <span class ="card-title">'. $p['mensagem'] .'</span><br>';
              echo '        </div>';
              echo '    </div>';
              echo '</div>';

              if ($i % 2 == 1) 
                  echo "</div>";  
                
              }
            }
              else{
            ?>
                <div class="row">
                    <div class="col s10 offset-s1">
                        <div class="card-panel cyan lighten-5 z-depth-5">
                            <span class="grey-text text-darken-3">
                                Você não possui postagem.
                            </span>
                        </div>                       
                    </div>
                </div>

        <?php
            }
        ?>
        </div>
      </main>
          <footer class="page-footer cyan grey-text darken-2-text">
            <div class="container"></div>
            <div class="footer-copyright">
              <div class="container">
              © 2018 Copyright FotoLog
              <a class="grey-text text-lighten-4 right" href="#!">More Links</a>
              </div>
            </div>
          </footer>  
        </div>
      </main>
      <script type="text/javascript" src="node_modules/jquery/dist/jquery.min.js"></script>
      <script type="text/javascript" src="node_modules/materialize-css/dist/js/materialize.min.js"></script>
    </body>
  </html>
        