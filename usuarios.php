<?php
  $arquivosUsuarios = 'usuarios.json';
  if (count($_POST)) {
    $usuario = $_POST;
    $usuarios = [];
    if (file_exists($arquivosUsuarios)) {
      $usuarios = json_decode(file_get_contents($arquivosUsuarios),true);
    }
    $usuarios[] = $usuario;
    file_put_contents($arquivosUsuarios, json_encode($usuarios));
  }
    $usuarios = [];
    if (file_exists($arquivosUsuarios)) {
      $usuarios = json_decode(file_get_contents($arquivosUsuarios),true);
    }

?>

<!DOCTYPE html>
<html>
    <head>
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link type="text/css" rel="stylesheet" href="./node_modules/materialize-css/dist/css/materialize.min.css" 
      media="screen,projection"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <style>
        body:{
          display: flex;
          min-height: 100vh;
          flex-direction: column;
        }
        main:{
          flex: 1 0 auto;
        }
      </style>
    </head>

  <body>
      <nav class="cyan">
        <div class="nav-wrapper">
          <a href="#" class="brand-logo center">FotoLog</a>
          <ul id="nav-mobile" class="left hide-on-med-and-down">
            <li><a href="postagem.php">Nova postagem</a></li>
            <li class="active"><a href="usuarios.php">Usuario</a></li>
            <li><a href="fotolog.php">Fotos</a></li>
          </ul>
        </div>
      </nav>

    <!--titulo -->
    <main>
        <div class="container ">
          <div class="row" style="margin-top: 50px;">
            <div class="col s8 offset-s2">
              <div class="card blue lighten-5">
                <div class="card-content">
                  <span class="card-title">Cadastre um novo usuario</span>
                  <br>

<?php
    
     if (count($usuarios)) {
        echo '<ul class="collection">';
      foreach ($usuarios as $u){
          echo '<li class="collection-item avatar">';
          echo '<i class="material-icons circle">account_circle</i>';
          echo '<span class="title">'.$u['nome'].'</span>';
          echo '<p>'.$u['email'].'<br> </p>';
          echo '<a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>';
          echo '</li>';
      }

      echo "</ul>";
     }

    else{
?>
    <div class="row">
        <div class="col s10 offset-s1">
            <div class="card cyan darken-2">
                <span class="white-text">
                    <p> <center>Voce nao possue nenhum usuario cadastrado</center> </p>
                </span>
        
        </div>
    </div>
            
<?php        
     }
?>
    </div>
    <!--formulario-->
      <div class="card-action">
        <form class="container" method="post">
          <div class="row">
            <div class="input-field col s6">
              <input placeholder="Nome ou apelido" id="nome" name="nome" type="text" class="validate">
              <label for="nome">Nome</label>
            </div>
            <div class="input-field col s6">
              <input placeholder="email@dominio" id="email" name="email" type="email" class="validate">
              <label for="email">E-mail</label>
            </div>
            <div class="col s12 right-align">
              <button class="btn waves-effect waves-ligth" type="submit">Cadastrar
                <i class="material-icons right">send</i>
          </div>
        </form>
      </div>
    </div>
</div>
</div>

</div> 
</main>

      <footer class="page-footer cyan grey-text darken-2-text">
        <div class="container"></div>
        <div class="footer-copyright">
          <div class="container">
          © 2018 Copyright FotoLog
          <a class="grey-text text-lighten-4 right" href="#!">More Links</a>
          </div>
        </div>
      </footer>  
      </div>
    </main>
    <script type="text/javascript" src="node_modules/jquery/dist/jquery.min.js"></script>
    <script type="text/javascript" src="node_modules/materialize-css/dist/js/materialize.min.js"></script>
  </body>
</html>
        