<?php
  $arquivosUsuarios  = 'usuarios.json';
  $arquivosPostagens = 'postagens.json';
  $erro_extensao_invalida = false;

    $usuarios = [];
    if (file_exists($arquivosUsuarios)) {
      $usuarios = json_decode(file_get_contents($arquivosUsuarios),true);
    }
    $postagens = [];
    if (file_exists($arquivosPostagens)) {
        $postagens = json_decode(file_get_contents($arquivosPostagens), true);
    }

    if (isset($_FILES['foto']) && count($_POST)) {
        $nome = $_FILES['foto']['name'];
        $tam  = $_FILES['foto']['size'];
        $tipo = $_FILES['foto']['type'];
        $tmp  = $_FILES['foto']['tmp_name'];
        $path = 'fotos/'. $nome;

        $extensoes = ["jpg","jpeg","png"];
        $ext       = explode('.', $nome);
        $extensao = strtolower(end($ext));
        $erro_extensao_invalida = ! in_array($extensao, $extensoes);

        if (!$erro_extensao_invalida && $tam > 0) {
             move_uploaded_file($tmp, $path);
             if (isset($_POST['usuario_id']) && ($_POST['usuario_id']< count($usuarios))){
                 $usuario = $usuarios[$_POST['usuario_id'] ]['nome'];
             }
             else{
                $usuario = 'Usuario desconhecido';
             }
            $titulo = $_POST['titulo'];
            $mensagem = $_POST['mensagem'];
            $foto     = $path;
            $novoPost = ['usuario' => $usuario, 'titulo'=>$titulo, 'mensagem'=>$mensagem, 'foto'=>$foto];
            $postagens[] = $novoPost;
            file_put_contents($arquivosPostagens, json_encode($postagens));
        }
    }
?>
<!DOCTYPE html>
  <html>
    <head>
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link type="text/css" rel="stylesheet" href="./node_modules/materialize-css/dist/css/materialize.min.css" 
      media="screen,projection"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <style>
        body:{
          display: flex;
          min-height: 100vh;
          flex-direction: column;
        }
        main:{
          flex: 1 0 auto;
        }
      </style>
    </head>

    <body>
      <nav class="cyan">
        <div class="nav-wrapper">
          <a href="#" class="brand-logo center">FotoLog</a>
          <ul id="nav-mobile" class="left hide-on-med-and-down">
            <li class="active"><a href="postagem.php">Nova postagem</a></li>
            <li><a href="usuarios.php">Usuario</a></li>
            <li><a href="fotolog.php">Fotos</a></li>
          </ul>
        </div>
      </nav>
      <main>
        <div class="container ">
          <div class="row" style="margin-top: 50px" >
            <div class="col s10 offset-s1"> 
              <div class="card blue lighten-5">
                  <div class="card-content">
                    <span class="card-title">Poste uma nova foto</span>
                    <br>
                    <form class="container" method="POST" enctype="multipart/form-data">
                       <div class="row">
                           <div class="input-field col s6">
                               <select name="usuario_id">
                                <?php
                                    if (count($usuarios)) {
                                        echo '<option value ="" disabled selected>Quem é você?</option>';
                                        foreach ($usuarios as $id => $u) {
                                            echo '<option value ="'.$id.'">'. $u['nome'].'</option>';
                                        }
                                    }
                                    else{
                                        echo '<option value ="" disabled selected>Cadastre um usuario</option>';
                                    }
                                ?>
                               </select>
                               <label>Usuario</label>
                           </div>
                           <div class="file-field input-field col s6">
                                <div class="btn cyan accent-4 col s2">
                                    <span><i class="material-icons center"> add_a_photo</i></span>
                                    <input type="file" name="foto">
                                </div>
                                <div class="file-path-wrapper col s10">
                                    <input class="file-path validate" type="text">
                                </div>
                           </div>
                       </div> 
                        <div class="row">
                           <div class="input-field col s12">
                                <label for="titulo">Titulo do post</label>
                                <input type="text" id="titulo" name="titulo">
                           </div>
                        </div>
                       <div class="row">
                           <div class="input-field col s12">
                             <label for="mensagem">Sua mensagem:</label>
                             <textarea id="mensagem" name="mensagem" class="materialize-textarea"></textarea>
                           </div>
                       </div>
                       <div class="row">
                           <div class="col s12 right-align">
                            <button class="btn waves-effect waves-ligth" type="submit">Submit
                               <i class="material-icons right">send</i>
                           </div>
                       </div>
                    </form>
                  </div>   
                </div>
              </div>
              <?php
                if ($erro_extensao_invalida) {
              ?>
                    <div class="row">
                        <div class="col s10 offset-s1">
                            <div class="card-panel red lighten-4">
                              <span class="grey-text text-darken-3">
                                  Erro: O arquivo que você enviou não é imagem valida .jpeg, jpg ou png.
                              </span>  
                            </div>
                        </div>
                    </div>
              <?php
                }
              ?>

            </div>
          </div>
          </main>
              <footer class="page-footer cyan grey-text darken-2-text">
                <div class="container"></div>
                <div class="footer-copyright">
                  <div class="container">
                  © 2018 Copyright FotoLog
                  <a class="grey-text text-lighten-4 right" href="#!">More Links</a>
                  </div>
                </div>
              </footer>  
            </div>
          </main>
          <script type="text/javascript" src="node_modules/jquery/dist/jquery.min.js"></script>
          <script type="text/javascript" src="node_modules/materialize-css/dist/js/materialize.min.js"></script>
          <script>
              $(document).ready(function(){
                $('select').material_select();
              });
          </script>
    </body>
  </html>
        